﻿using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;
using Photon.Pun;
using System.Collections.Generic;

namespace Sber.Pong
{
    public class ScorePanel : MonoBehaviourPunCallbacks
    {
        [SerializeField] public Text player_1;
        [SerializeField] public Text player_2;

        #region UNITY

        public void Awake()
        {
            foreach (Player p in PhotonNetwork.PlayerList)
                SetScore(p);
        }

        private void SetScore(Player player)
        {
            if (player.ActorNumber == 1)
            {
                player_1.text = player.GetScore().ToString();
            }
            else if (player.ActorNumber == 2)
            {
                player_2.text = player.GetScore().ToString();
            }
        }

        #endregion

        #region PUN CALLBACKS

        public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
        {
            SetScore(targetPlayer);
        }

        #endregion
    }
}
