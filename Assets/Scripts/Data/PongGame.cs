﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sber.Pong
{
    public class PongGame 
    {
        public const string PLAYER_READY = "IsPlayerReady";
        public const string PLAYER_LOADED_LEVEL = "PlayerLoadedLevel";

        public const string GAME_FINISH_SCORE = "FinishScore";
        public static int DEFAULT_MAX_SCORE = 7;

        public const string GAME_BALL_SPEED = "BallSpeed";
        public static float DEFAULT_BALL_SPEED = 4;

        public const string GAME_RACKET_SPEED = "RacketSpeed";
        public static float DEFAULT_RACKET_SPEED = 5;

        public const string LEVEL_LOBBY= "Lobby";
        public const string LEVEL_GAMEAREA = "GameArea";

        public static Color GetColor(int colorChoice)
        {
            switch (colorChoice)
            {
                case 0: return Color.red;
                case 1: return Color.green;
            }

            return Color.black;
        }
    }
}