﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sber.Pong
{
    public class PlayerRacket : AbstractRacket
    {
        protected new void Start()
        {
            base.Start();

            PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(PongGame.GAME_RACKET_SPEED, out object _speed);
            speed = (float)_speed;
        }
    }
}