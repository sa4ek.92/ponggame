﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Sber.Pong
{
    [RequireComponent(typeof(Rigidbody2D))]
    public abstract class AbstractRacket : MonoBehaviour
    {
        [SerializeField] protected float speed = 1f;
        protected Rigidbody2D _rigidbody;

        protected void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();

            PhotonView photonView = GetComponent<PhotonView>();

            if (photonView.IsMine)
                GetComponent<Renderer>().material.color = PongGame.GetColor(0);
            else
                GetComponent<Renderer>().material.color = PongGame.GetColor(1);
        }

        protected void Update()
        {
            if (Input.GetKey(KeyCode.DownArrow))
            {
                _rigidbody.velocity = speed * Vector2.down;
            }
            else if (Input.GetKey(KeyCode.UpArrow))
            {
                _rigidbody.velocity = speed * Vector2.up;
            }
            else if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                _rigidbody.velocity = (pos - transform.position) * speed;
            }
            else
            {
                _rigidbody.velocity = Vector2.zero;
            }
        }
    }
}