﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sber.Pong
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Ball : MonoBehaviour
    {

        [SerializeField] public float ballSpeed = 1f;

        Rigidbody2D _rigidbody;


        void Start()
        {
            PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(PongGame.GAME_BALL_SPEED, out object _speed);
            ballSpeed = (float)_speed;

            _rigidbody = GetComponent<Rigidbody2D>();

            Vector2 randomVector = Random.insideUnitCircle;
            randomVector.Normalize();
            if (randomVector.y < 0.1)
                randomVector = new Vector2(0.5f, 0.5f); ;

            _rigidbody.velocity = ballSpeed * randomVector;
        }

        public void Sleep()
        {
            if (PhotonNetwork.IsMasterClient)
                _rigidbody.Sleep();
        }

        public void WakeUp()
        {
            if (PhotonNetwork.IsMasterClient)
                _rigidbody.WakeUp();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (PhotonNetwork.IsMasterClient)
                PhotonNetwork.Destroy(gameObject);

            GameManager.instance.OnGateEntered(collision.gameObject);
        }
    }
}