﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class PlayerNetworking : MonoBehaviour
{
    [SerializeField] List<MonoBehaviour> scriptsToIgnore = new List<MonoBehaviour>();

    private PhotonView photonView;

    void Start()
    {
        scriptsToIgnore.Add(this);

        photonView = GetComponent<PhotonView>();

        if (!photonView.IsMine)
        {
            foreach(MonoBehaviour behaviour in scriptsToIgnore)
            {
                behaviour.enabled = false;
            }
        }
    }
}
