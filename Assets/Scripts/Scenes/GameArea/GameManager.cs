﻿using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace Sber.Pong
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        public static GameManager instance = null;

        [SerializeField] public GameObject ballPrefab;
        [SerializeField] public GameObject playerPrefab;

        [SerializeField] public GameObject InfoPanel;
        [SerializeField] public Text InfoText;

        [SerializeField] public GameObject SettingsButton;
        [SerializeField] public GameObject SettingsPanel;
        [SerializeField] public InputField BallSpeedInputField;

        [SerializeField] public GameObject ButtonPlayAgain;

        private Player player_1;
        private Player player_2;

        private GameObject ball;
        private GameObject racket_1;
        private GameObject racket_2;


        private bool create_balls = true;
        #region UNITY

        public void Awake()
        {
            instance = this;
        }

        public override void OnEnable()
        {
            base.OnEnable();

            CountdownTimer.OnCountdownTimerHasExpired += OnCountdownTimerIsExpired;
        }

        public void Start()
        {
            if (PhotonNetwork.IsMasterClient)
                PhotonNetwork.DestroyAll();

            Hashtable props = new Hashtable
            {
                {PongGame.PLAYER_LOADED_LEVEL, true}
            };

            PhotonNetwork.LocalPlayer.SetCustomProperties(props);
        }

        public override void OnDisable()
        {
            base.OnDisable();

            CountdownTimer.OnCountdownTimerHasExpired -= OnCountdownTimerIsExpired;
        }

        #endregion

        #region UI
        public void OnExitButtonPressed()
        {
            OnLeftRoom();
        }

        public void OnRestart()
        {
            player_1.SetScore(0);
            player_2.SetScore(0);

            StartCoroutine(ReloaSceneCor());
        }

        IEnumerator ReloaSceneCor()
        {
            GetComponent<PhotonView>().RPC("LoadMyScene", RpcTarget.Others, SceneManager.GetActiveScene().name);
            yield return null;
            PhotonNetwork.IsMessageQueueRunning = false;
            PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.LocalPlayer);
            PhotonNetwork.LoadLevel(SceneManager.GetActiveScene().name);
        }

        [PunRPC]
        public void LoadMyScene(string sceneName)
        {
            PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.LocalPlayer);
            PhotonNetwork.LoadLevel(sceneName);
        }

        public void ShowSettings()
        {
            if (ball)
                ball.GetComponent<Ball>().Sleep();

            SettingsPanel.SetActive(true);
        }

        public void HideSettings()
        {
            float.TryParse(BallSpeedInputField.text, out float ballSpeed);
            ballSpeed = (ballSpeed < 1) ? PongGame.DEFAULT_BALL_SPEED : ballSpeed;
            ballSpeed = Mathf.Clamp(ballSpeed, 1, 10);

            Hashtable props = new Hashtable
            {
                {PongGame.GAME_BALL_SPEED, ballSpeed},
            };

            PhotonNetwork.CurrentRoom.SetCustomProperties(props);

            SettingsPanel.SetActive(false);

            if (ball)
                ball.GetComponent<Ball>().WakeUp();

            OnRestart();
        }

        #endregion

        #region PUN CALLBACKS

        public override void OnDisconnected(DisconnectCause cause)
        {
            SceneManager.LoadScene(PongGame.LEVEL_LOBBY);
        }

        public override void OnLeftRoom()
        {
            PhotonNetwork.Disconnect();
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            OnLeftRoom();
        }

        public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
        {
            CheckEndOfGame();

            if (!PhotonNetwork.IsMasterClient)
            {
                return;
            }

            InfoText.text = "";
            if (changedProps.ContainsKey(PongGame.PLAYER_LOADED_LEVEL))
            {
                if (CheckAllPlayerLoadedLevel())
                {
                    Hashtable props = new Hashtable
                    {
                        {CountdownTimer.CountdownStartTime, (float) PhotonNetwork.Time}
                    };
                    PhotonNetwork.CurrentRoom.SetCustomProperties(props);
                }
            }
        }

        #endregion

        private void OnCountdownTimerIsExpired()
        {
            InfoPanel.SetActive(false);
            StartGame();
        }

        public void StartGame()
        {
            create_balls = true;

            ButtonPlayAgain.SetActive(false);

            foreach (Player p in PhotonNetwork.PlayerList)
            {
                if (p.GetPlayerNumber() == 0)
                {
                    player_1 = p;
                }
                else if (p.GetPlayerNumber() == 1)
                {
                    player_2 = p;
                }
            }

            if (PhotonNetwork.IsMasterClient && (!PhotonNetwork.LocalPlayer.Equals(player_1) || !PhotonNetwork.LocalPlayer.Equals(player_2)))
            {
                SettingsButton.SetActive(true);
            }

            if (PhotonNetwork.IsMasterClient)
            {
                if (ball != null)
                    PhotonNetwork.Destroy(ball);
                if (racket_1 != null)
                    PhotonNetwork.Destroy(racket_1);
                if (racket_2 != null)
                    PhotonNetwork.Destroy(racket_2);

                ball = PhotonNetwork.Instantiate("Ball", Vector3.zero, Quaternion.identity);
            }

            if (PhotonNetwork.LocalPlayer.GetPlayerNumber() == 0)
                racket_1 = PhotonNetwork.Instantiate("Player", Vector3.zero, Quaternion.identity);
            else if (PhotonNetwork.LocalPlayer.GetPlayerNumber() == 1)
                racket_2 = PhotonNetwork.Instantiate("Player", Vector3.zero, Quaternion.identity * Quaternion.Euler(0, 180f, 0));

        }

        private bool CheckAllPlayerLoadedLevel()
        {
            foreach (Player p in PhotonNetwork.PlayerList)
            {
                object playerLoadedLevel;

                if (p.CustomProperties.TryGetValue(PongGame.PLAYER_LOADED_LEVEL, out playerLoadedLevel))
                {
                    if ((bool)playerLoadedLevel)
                    {
                        continue;
                    }
                }

                return false;
            }

            return true;
        }

        private void CheckEndOfGame()
        {
            bool max_score_completed = false;

            PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(PongGame.GAME_FINISH_SCORE, out object max_score);

            foreach (Player p in PhotonNetwork.PlayerList)
            {
                if (p.GetScore() >= (int)max_score)
                {
                    max_score_completed = true;
                    break;
                }
            }

            if (max_score_completed)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    StopAllCoroutines();
                }

                string winner = "";
                int score = -1;

                foreach (Player p in PhotonNetwork.PlayerList)
                {
                    if (p.GetScore() > score)
                    {
                        winner = p.NickName;
                        score = p.GetScore();
                    }
                }

                InfoPanel.SetActive(true);
                InfoText.text = string.Format("Игрок {0} победил", winner);
                if (PhotonNetwork.IsMasterClient)
                    ButtonPlayAgain.SetActive(true);
                create_balls = false;
            }
        }

        public void OnGateEntered(GameObject gate)
        {
            if (!PhotonNetwork.IsMasterClient)
                return;

            switch (gate.name)
            {
                case "LeftGate":
                    player_1.AddScore(1);
                    break;
                case "RightGate":
                    player_2.AddScore(1);
                    break;
                default:
                    break;
            }

            if (create_balls)
                PhotonNetwork.Instantiate("Ball", Vector3.zero, Quaternion.identity);
        }
    }
}